#include <stdio.h>
#include <stdlib.h>


void evaluar(int n, double *coeficiente){
    double x, resultado=0.00, temp;

    printf("\nIngrese el punto X a evaluar: ");
    printf("\t");
    scanf("%lf", &x);

    for (int i = 0; i <= n; i++) { 
        if(i==n){
            temp=1;
        }else{
            temp=1;
            for(int j=0;j<n-i;j++){
                temp=temp*x;
        }  
        }
        resultado=resultado+(coeficiente[i]*temp);
    }
    
    printf("\nResultado : %.2lf",resultado);
    printf("\n");      
}
int main(){
    int n;
    double ingreso,*coeficiente;
    printf("Ingrese grado del polinomio: ");
    scanf("%d", &n);

   
    coeficiente =(double* ) malloc(n *sizeof(int)+1);

    
    for (int i = 0; i < n+1; i++){
        printf("Ingrese coeficiente para X^%d",n-i);
        printf("\t");
        scanf("%lf", &ingreso);
        coeficiente[i] = ingreso;
    }

    printf("\nPolinomio ingresado: \n");
    for (int i = 0; i < n+1; i++){
        printf(" %.2lf", coeficiente[i]);
        printf(" X^%d",n-i);
    }

    printf("\n");
    evaluar(n,coeficiente);
    return 0;

}

